<?php
/**
 * Created by PhpStorm.
 * User: kate
 * Date: 09/07/18
 * Time: 11:38
 */
interface TravelType
{
    public function countPrice(int $time);

}
//examples of how to count a price for a journey depending on time of travel and a vehicle. We predent a vehicle coefficients as 5 for a bus,10 for a car and 100 for a plane(just for an example)
class ByBus implements TravelType
{
    
    public function countPrice(int $time){
       
        return $time*5;
    }

}
class ByCar implements TravelType
{
    public function countPrice(int $time){
        return $time * 10;
    }
}

class ByPlane implements TravelType
{
    public function countPrice(int $time){

        return $time * 100;
    }
}

class TravelAgent
{
    private $travel_type;
    private $time;


    public function setTravelType(TravelType $travel_type)
    {
        $this->travel_type = $travel_type;
        return $this;
    }
    public function getTravelType(TravelType $travel_type)
    {
        $this->travel_type = $travel_type;
        return $this->travel_type;
    }

    public function getTime()
    {
        return $this -> time;
    }


    public function getPrice(int $time)
    {
         $this->time = $time;
        return $this->getTravelType($this->travel_type)->countPrice($this->getTime());
    }

}

$travel_agent = new TravelAgent();

echo $priceFromBus = $travel_agent->setTravelType(new ByBus())->getPrice(5);
echo ' ';
echo $priceFromCar = $travel_agent->setTravelType(new ByCar())->getPrice(4);
echo ' ';
echo $priceFromPlane = $travel_agent->setTravelType(new ByPlane())->getPrice(1);
